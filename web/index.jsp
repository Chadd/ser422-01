<%@page import="edu.asu.ser422.assign1.model.News"%>
<%@page import="java.util.Arrays"%>
<%
	String uid = (String) request.getAttribute("uid");
	if (uid==null) uid = "";
	String uRole = (String) request.getAttribute("role");
	if (uRole==null) uRole = "";
%>
<html>
	<head>
		<title> New News Inc. </title>
	</head>
	<body>
		<h1>New News Inc.</h1>
		<%
			if (uid == null || uid.length() < 1) {
		%>
		<h3> Welcome guest, please (<a href='login'>log in</a>) </h3>
		<%
			} else {
		%>
		<h3> Welcome back <%=uid %>, you are currently signed in as a <%= uRole %>.
			 (<a href='login?action=logout'>log out</a>) 
			 (<a href='edit'>Create new news</a>)
		<%
			}
		%>
		<ul>
			<%
				News[] newsItems = (News[]) request.getAttribute("news");
				for (News news : newsItems) {
			%>
			<li>
				<table>
					<tr>
						<td>
							<a href='read?title=<%= news.getEncodedTitle() %>'><%= news.title %></a>
						</td>
						<td>
							<% if (uRole.equals("reporter")) { %>
							<form action='edit' method='GET'>
							<input type='hidden' name='title' value='<%= news.getEncodedTitle()%>' />
							<input type='submit' value='Edit' />
							</form>
							<form action='rm' method='GET'>
							<input type='hidden' name='title' value='<%= news.getEncodedTitle() %>'>
							<input type='submit' value='Delete' />
							</form>
							<% } %>
						</td>
					</tr>
				</table>
			</li>
			<%
				}			
			%>
		</ul>
	</body>
</html>
