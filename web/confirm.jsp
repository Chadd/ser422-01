<%@page import="java.util.Arrays"%>
<%
	// Data fields
	String uid = (String) request.getAttribute("uid");
	if (uid==null) uid = request.getParameter("uid");
	if (uid==null) uid = "";
	String title = (String) request.getAttribute("title");
	if (title==null) title = request.getParameter("title");
	if (title==null) title = "";
	String comment = (String) request.getAttribute("comment");
	if (comment==null) comment = request.getParameter("comment");
	if (comment==null) comment = "";
	String message = (String) request.getAttribute("message");
%>
<html>
	<head>
		<title> Confirm action </title>
	</head>
	<body>
		<h1>Confirm action</h1>
		<% if (message != null) { %>
			<h3><%=message%></h3>
		<% } %>
		<form action='rm' method='POST'>
		<% if (title.length() > 0) {%>
			<input type='hidden' name='title' value='<%=title %>'>
		<% } %>
		<% if (comment.length() > 0) {%>
			<input type='hidden' name='comment' value='<%=comment %>'>
		<% } %>
		<input type='hidden' name='confirm' value='yes'>
		<input type='submit' value='Continue' />
		</form>
		<form action='index' method='GET'>
		<input type='submit' value='Cancel' />
		<%%>
	</body>
</html>
