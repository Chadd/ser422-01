<%@page import="java.util.Arrays"%>
<%
	// Data fields
	String uid = request.getParameter("uid");
	if (uid==null) uid = (String) request.getAttribute("uid");
	if (uid==null) uid = "";
	String action = (String) request.getAttribute("action");
	if (action==null) action = request.getParameter("action");
	if (action==null) action = "";
	String message = (String) request.getAttribute("message");
%>
<html>
	<head>
		<title> Log In </title>
	</head>
	<body>
		<h1>Log In</h1>
		<% if (message != null) { %>
			<h3><%=message%></h3>
		<% } %>
		<p>
				<form action="login" method="POST">
				<h3>Name</h3>
				<p><input type="text" name="uid" value='<%=uid %>' /></p>
				<h3>Password</h3>
				<p><input type="password" name="upw" value='<%=uid %>' /></p>
				<% if (action.equals("create")) { %>
					<h3>The account you specified does not exist. Select a role to create this account</h3>
					<input type="hidden" name="action" value="create" />
					<select name="role" size="1">
					  <option>subscriber</option>
					  <option>reporter</option>
					  </select></p>
					<input type="submit" value="Create"/>
				<% }  else {%>
					<input type="hidden" name="action" value="login" />
					<input type="submit" value="Login"/>
				<% } %>
				
			</form>
		</p>
		<%%>
	</body>
</html>
