<%@page import="edu.asu.ser422.assign1.model.News"%>
<%@page import="java.util.Arrays"%>
<%
	String uid = (String) request.getAttribute("uid");
	if (uid==null) uid = "";
	String uRole = (String) request.getAttribute("role");
	if (uRole==null) uRole = "";
%>
<html>
	<head>
		<title> New News Inc. </title>
	</head>
	<body>
		<h1>New News Inc.</h1>
		<%
			if (uid == null || uid.length() < 1) {
		%>
		<h3> Welcome guest, please (<a href='login'>log in</a>) </h3>
		<%
			} else {
		%>
		<h3> Welcome back <%=uid %>, you are currently signed in as a <%= uRole %>.
			 (<a href='login?action=logout'>log out</a>) 
			 (<a href='edit'>Create new news</a>)
		<%
			}
		%>
		<%
			News[] newsItems = (News[]) request.getAttribute("news");
			if (newsItems != null && newsItems.length > 0) {
		%>
			<h3><%=newsItems[0].title%></h3>
			<p>Written by <%=newsItems[0].author%></p>
			<p><%=newsItems[0].body%></p>
			<h2>Comments</h2>
			<ul>
			<% for (String comment : newsItems[0].comments) {%>
				<li>
					<%=comment %>
					<%if (uid.equals(newsItems[0].author)) {%>
						<form action='rm' method='POST'>
						<input type='hidden' name='title' value='<%=newsItems[0].title %>' />
						<input type='hidden' name='comment' value='<%=comment %>' />
						<input type='submit' value='Delete' />
						</form>
					<% } %>
				</li>
			<% } %>
			</ul>
			<p> Add a comment: <form method='POST' action='comment'>
			<input type='text' name='comment' value='' />
			<input type='hidden' name='title' value='<%=newsItems[0].title%>' />
			<input type='submit' value='Comment' />
			</p>
		<% } else {%>
			<p>
				Sorry, but I could not find the article you requested.
				 It may have been recently deleted or moved.
			</p>
		<% } %>
		
	</body>
</html>
