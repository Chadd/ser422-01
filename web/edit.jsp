<%@page import="edu.asu.ser422.assign1.model.News"%>
<%@page import="java.util.Arrays"%>
<%
	String uid = (String) request.getAttribute("uid");
	if (uid==null) uid = "";
	String uRole = (String) request.getAttribute("role");
	if (uRole==null) uRole = "";
	News[] newsItems = (News[]) request.getAttribute("news");
%>
<html>
	<head>
		<title> New News Inc. </title>
	</head>
	<body>
		<h1>News editor</h1>
		<%
			if (uid == null || uid.length() < 1) {
		%>
		<h3> Welcome guest, please (<a href='login'>log in</a>) </h3>
		<%
			} else {
		%>
		<h3> Welcome back <%=uid %>, you are currently signed in as a <%= uRole %>.
			 (<a href='login?action=logout'>log out</a>) 
			 (<a href='edit'>Create new news</a>)
		<%
			}
		%>
		<form method='POST' action='edit'>
		<%
			if (newsItems != null && newsItems.length > 0) {
		%>
			<h3><%=newsItems[0].title %></h3>
			<% if (newsItems[0].published) { %>
				<p><input name='published' type='checkbox' checked=true /> Published</p>
			<% } else { %>
				<p><input name='published' type='checkbox' /> Published</p>
			<% } %>
			<textarea name='body'rows='10' cols='50'><%=newsItems[0].body %></textarea>
			<input type='hidden' name='title' value='<%=newsItems[0].title %>' />
			<input type='hidden' name='action' value='save' />
			<br />
			<input type='submit' value='Save' /></form>
		<%
			} else {
		%>
			<p>Title: <input type='text' name='title' value='' /></p>
			<p><input name='published' type='checkbox' checked=true /> Published</p>
			<textarea name='body'rows='10' cols='50'></textarea>
			<input type='hidden' name='action' value='save' />
			<br />
			<input type='submit' value='Create' /></form>
		<% } %>
		</form>
	</body>
</html>
