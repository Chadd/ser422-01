package edu.asu.ser422.assign1.service;

import edu.asu.ser422.assign1.model.News;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class NewsDAO {
	
	private DataService myService;
	
	public NewsDAO() {
		myService = null;
	}
	
	public NewsDAO(DataService aDataService) {
		this.myService = aDataService;
	}
	
	/**
	 * A non-authenticated news get. Because no aith is provided,
	 * this method will only return articles that a non-authed
	 * user would have access to
	 */
	public News[] getNews() {
		return this.getNews("","");
	}
	
	public News[] getNews(String uid, String role) {
		List<News> lstReturn = new ArrayList<News>();
		News[] resultSet = myService.getNewsByContext(uid,role);
		if (resultSet != null) {
			lstReturn.addAll(Arrays.asList(resultSet));
		}
		return lstReturn.toArray(new News[0]);
	}
	
	public News[] getNewsByTitle(String title) {
		News[] resultSet = myService.getNewsByTitle(title);
		return resultSet;
	}
	public void createNews(News someNews) {
		myService.createNews(someNews);
	}
	
	public void deleteNews(String title) {
		this.myService.deleteNews(title);
	}
	
	public void createComment(String articleTitle, String comment) {
		this.myService.createComment(articleTitle, comment);
	}
	
	public void deleteComment(String articleTitle, String comment) {
		this.myService.deleteComment(articleTitle, comment);
	}
}
