package edu.asu.ser422.assign1.service;

import edu.asu.ser422.assign1.model.News;
import edu.asu.ser422.assign1.model.NewsUser;

public interface DataService {
		
	// Users CRUD
	void createUser(String uid, String role);
	NewsUser getUser(String uid);

	// News CRUD
	void createNews(News someNews);
	News[] getNews();
	News[] getNewsByUid(String uid);
	News[] getNewsByTitle(String title);
	News[] getNewsByPublished(boolean isPublished);
	News[] getNewsByContext(String uid, String role);
	void deleteNews(String title);
	
	// Comments CRUD
	void createComment(String articleTitle, String comment);
	void deleteComment(String articleTitle, String comment);
	
	// Used for debugging
	String typeAsString();
}
