package edu.asu.ser422.assign1.service;

import edu.asu.ser422.assign1.model.*;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class SqlDataService implements DataService {
	
	public SqlDataService() {
		
	}
	
	public SqlDataService(SqlConfig aConfig) {
		
	}
	
	// Implementations of interface methods are in order below
	// Users CRUD
	public void createUser(String uid, String role) {
		
	}
	public NewsUser getUser(String uid) {
		return new NewsUser();
	}
	
	// News CRUD
	public News[] getNews() {
		List<News> lstReturn = new ArrayList<News>();
		
		return lstReturn.toArray(new News[0]);
	}
	public News[] getNewsByUid(String uid) {
		List<News> lstReturn = new ArrayList<News>();
		
		return lstReturn.toArray(new News[0]);
	}
	public News[] getNewsByTitle(String title) {
		List<News> lstReturn = new ArrayList<News>();
		
		return lstReturn.toArray(new News[0]);
	}
	
	public News[] getNewsByPublished(boolean isPublished) {
		List<News> lstReturn = new ArrayList<News>();

		return lstReturn.toArray(new News[0]);		
	}
	
	public News[] getNewsByContext(String uid, String role) {
		return new News[0];
	}
	
	public void createNews(News someNews) {
		
	}
	public void deleteNews(String title) {
		
	}
	
	// Comments CRUD
	public void createComment(String articleTitle, String comment) {
		
	}

	public void deleteComment(String articleTitle, String comment) {
		
	}
	
	// Used for debugging
	public String typeAsString() {
		return "SQL";
	}
}
