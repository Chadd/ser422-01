package edu.asu.ser422.assign1.service;

import edu.asu.ser422.assign1.model.NewsUser;

public class NewsUserDAO {
	
	private DataService myService;
	
	public NewsUserDAO() {
		myService = null;
	}
	
	public NewsUserDAO(DataService aDataService) {
		this.myService = aDataService;
	}
	
	public void createUser(String uid, String role) {
		this.myService.createUser(uid,role);
	}
	
	public NewsUser getUser(String uid) {
		return this.myService.getUser(uid);
	}
	
}
