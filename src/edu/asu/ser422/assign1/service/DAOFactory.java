package edu.asu.ser422.assign1.service;

import java.io.*;
import javax.servlet.ServletContext;
import javax.servlet.http.*;
import com.google.gson.JsonSyntaxException;
import java.text.SimpleDateFormat;
import java.net.URL;
import java.util.Properties;

public class DAOFactory {
	
	private DataService dataService;
	private Properties config;
	private ServletContext thisContext;
	
	public DAOFactory() {
		this.dataService = null;
		this.config = new Properties();
		this.thisContext = null;
	}
	
	public DAOFactory(ServletContext aContext, String configPath) {
		this();
		
		// Hold on to context, might need it later
		this.thisContext = aContext;
		
		// Attempt to read the properties file
		InputStream is = thisContext.getResourceAsStream(configPath);
		boolean loadFailed = false;
		try {
			config.load(is);
		} catch (IOException e) {
			System.out.println("ERROR: Unable to read properties file");
			System.out.println("ERROR:   ->   path=" + configPath);
			System.out.println("ERROR:   ->  rPath=" + thisContext.getRealPath(configPath));
			
			e.printStackTrace();
			loadFailed = true;
		} catch (Exception exception) {
			System.out.println("ERROR: Unknown exception when trying to load properties file");
			System.out.println("ERROR:   ->  path=" + configPath);
			System.out.println("ERROR:   ->  rPath=" + thisContext.getRealPath(configPath));
			System.out.println("ERROR:   ->  ex=" + exception.toString());
			
			exception.printStackTrace();
			loadFailed = true;
		} finally {
			try {
					is.close();
			} catch (IOException ex) {
					System.out.println("WARNING: Unable to cose input stream");
			}
		}
		
		if (loadFailed) {
			config = new Properties();
		}
		this.configure();
	}
	
	/**
	 * Configures the DataService used by the DAOFactory to
	 * create DAO's. It will try to use data sources in the
	 * following order;
	 * 
	 * 1. The source specified in the properties file
	 * 2. A Null data source, which returns empty result sets
	 */
	private void configure() {
		String selectedSource = "null";
		if (config != null) {
			selectedSource = config.getProperty("source","null");
		}
		
		// Configure respective sources
		if (selectedSource.contains("file")) {
			
			// Get file locations for this source
			FlatFileConfig fileConfig = new FlatFileConfig();
			fileConfig.newsPath = config.getProperty("news_file","");
			fileConfig.userPath = config.getProperty("users_file","");
			fileConfig.aContext = thisContext;
			
			if (fileConfig.appearsValid()) {
				dataService = new FlatFileDataService(fileConfig);
			} else {
				System.out.println("ERROR: Flat file data service failed to validate");
			}
			
		} else if (selectedSource.contains("sql")) {
			
			// Get needed configuration values
			SqlConfig dbConfig = new SqlConfig();
			dbConfig.host = config.getProperty("host","");
			dbConfig.port = config.getProperty("port","");
			dbConfig.user = config.getProperty("user","");
			dbConfig.password = config.getProperty("password","");
			dbConfig.db_name = config.getProperty("db_name","");
			dbConfig.table_prefix = config.getProperty("table_prefix","");
			
			if (dbConfig.appearsValid()) {
				dataService = new SqlDataService(dbConfig);
			} else {
				System.out.println("ERROR: SQL data service failed to validate");
			}
		}
		
		// If data source has still not been defined,
		// fall back to a null providing source
		if (dataService == null) {
			dataService = new NullDataService();
			System.out.println("WARNING: Data source fell back to a null provider");
		}
	}
	
	public boolean isConfigured() {
		boolean bolReturn = false;
		if (dataService != null) bolReturn = true;
		return bolReturn;
	}
	
	public NewsDAO getNewsDAO() {
		NewsDAO daoReturn = new NewsDAO(dataService);
		return daoReturn;
	}
	
	public NewsUserDAO getNewsUserDAO() {
		NewsUserDAO daoReturn = new NewsUserDAO(dataService);
		return daoReturn;
	}
	
	public String getDataStoreType() {
		return dataService.typeAsString();
	}

}
