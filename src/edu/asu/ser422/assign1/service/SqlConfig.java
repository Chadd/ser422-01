package edu.asu.ser422.assign1.service;

public class SqlConfig {
	public String host;
	public String port;
	public String user;
	public String password;
	public String db_name;
	public String table_prefix;
	
	public SqlConfig() {
		this.host = "";
		this.port = "";
		this.user = "";
		this.password = "";
		this.db_name = "";
		this.table_prefix = "";
	}
	
	public boolean appearsValid() {
		boolean bolReturn = true;
		if (this.host == null || this.host.length() < 1) {
			bolReturn = false;
		}
		
		if (this.port == null || this.port.length() < 1) {
			bolReturn = false;
		}
		
		if (this.user == null || this.user.length() < 1) {
			bolReturn = false;
		}
		
		if (this.password == null || this.password.length() < 1) {
			bolReturn = false;
		}
		
		if (this.db_name == null || this.db_name.length() < 1) {
			bolReturn = false;
		}
		
		if (this.table_prefix == null || this.table_prefix.length() < 1) {
			bolReturn = false;
		}
		return bolReturn;
	}
}
