package edu.asu.ser422.assign1.service;

import javax.servlet.ServletContext;

public class FlatFileConfig {
	public String newsPath;
	public String userPath;
	public ServletContext aContext;
	
	public FlatFileConfig() {
		newsPath = "";
		userPath = "";
		aContext = null;
	}
	
	public boolean appearsValid() {
		boolean bolReturn = true;
		if (newsPath == null || newsPath.length() < 1) {
			bolReturn = false;
		}
		
		if (userPath == null || userPath.length() < 1) {
			bolReturn = false;
		}
		
		if (aContext == null) {
			bolReturn = false;
		}
		return bolReturn;
	}
}
