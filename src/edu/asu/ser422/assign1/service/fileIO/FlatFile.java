package edu.asu.ser422.assign1.service.fileIO;

import java.io.*;

public interface FlatFile {
	void save(File outFile);
	void load(InputStream inFile);
}
