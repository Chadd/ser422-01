package edu.asu.ser422.assign1.service.fileIO;

import edu.asu.ser422.assign1.model.NewsUser;

import java.io.*;
import com.google.gson.*;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class NewsUsersFile implements FlatFile {
	
	private List<NewsUser> newsUserList;
	
	public NewsUsersFile() {
		newsUserList = new ArrayList<NewsUser>();
	}
	
	public NewsUsersFile(InputStream inFile) {
		this();
		load(inFile);			
	}
	
	public NewsUsersFile(NewsUser[] someNewsUsers) {
		List<NewsUser> aList = Arrays.asList(someNewsUsers);
		newsUserList = new ArrayList<NewsUser>(aList);
	}
	
	public void createUser(String uid, String role) {
		NewsUser newUser = new NewsUser(uid,role);
		newsUserList.add(newUser);
	}
	
	public NewsUser getUser(String uid) {
		NewsUser usrReturn = null;
		for (NewsUser aUser : newsUserList) {
			if (aUser.uid.equals(uid)) usrReturn = aUser;
		}
		return usrReturn;
	}
	
	public void save(File outFile) {
		byte[] outBytes = this.toJSON().getBytes();
		FileOutputStream fOut = null;
		try {
			fOut = new FileOutputStream(outFile, false);
			fOut.write(outBytes);
			fOut.close();
		} catch (Exception e) {
			System.out.println("WARNING: Save of news data failed");
			if (fOut != null) {
				try {
					fOut.close();
				} catch (IOException ex) {
					System.out.println("WARNING: Unable to cose output stream");
				}
			}
		}
	}
	
	public void load(InputStream inFile) {
		String jsonRaw = loadFromFile(inFile);
		
		if (jsonRaw != null) {
			List<NewsUser> aList = Arrays.asList(parseJSON(jsonRaw));
			newsUserList = new ArrayList<NewsUser>(aList);
		}	
	}
	
	private NewsUser[] parseJSON(String jsonData) throws JsonSyntaxException {
		NewsUser[] arrUsers;
		Gson gson = new Gson();
		arrUsers = gson.fromJson(jsonData, NewsUser[].class);
		return arrUsers;
	}
	
	private String toJSON() {
		String strReturn = "";
		Gson gson = new Gson();
		strReturn = gson.toJson(newsUserList.toArray());
		return strReturn;
	}
	
	private String loadFromFile(InputStream is) {
		StringBuffer jsonData = new StringBuffer();
		BufferedReader bRead = null;
		try {
			bRead = new BufferedReader(new InputStreamReader(is));
			String line = "";
			while((line = bRead.readLine())!=null){
				jsonData.append(line.trim());
			}
		} catch (IOException e) {
			System.out.println("FATAL: IOException");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("FATAL: Exception");
			e.printStackTrace();
		} finally {
			if (bRead != null) {
				try {
					bRead.close();
				} catch (IOException e) {
					System.out.println("WARNING: Unable to cose input stream");
				}
			}
		}
		
		return jsonData.toString();
	}
	
}
