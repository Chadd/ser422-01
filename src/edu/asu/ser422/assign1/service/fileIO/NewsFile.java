package edu.asu.ser422.assign1.service.fileIO;

import edu.asu.ser422.assign1.model.News;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import javax.xml.parsers.*;
import java.io.*;
import org.xml.sax.*;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.helpers.DefaultHandler;
import java.util.Arrays;

public class NewsFile extends DefaultHandler implements FlatFile  {
	
	private List<News> newsList;
	private String lastElement;
	private String elementTrace;
	
	public NewsFile() {
		newsList = new ArrayList<News>();
		lastElement = "";
		elementTrace = "";
	}
	
	public NewsFile(InputStream inFile) {
		this();
		load(inFile);
	}
	
	public NewsFile(News[] someNews) {
		List<News> aList = Arrays.asList(someNews);
		newsList = new ArrayList<News>(aList);
	}
	
	@Deprecated
	public void addNews(String aTitle, String anAuthor, String aBody, boolean isPublished) {
		News newNews = new News(aTitle, anAuthor, aBody, isPublished);
		newsList.add(newNews);
	}
	
	public News[] getNews() {
		return newsList.toArray(new News[0]);
	}
	
	public void createNews(News someNews) {
		boolean bolReplace = false;
		int replaceIndex = 0;
		
		for (News news : newsList) {
			if (news.titleEquals(someNews.title)) {
				bolReplace = true;
				replaceIndex = newsList.indexOf(news);
			}
		}
		
		if (bolReplace) {
			newsList.set(replaceIndex, someNews);
		} else {
			newsList.add(someNews);
		}
	}
	
	public void deleteNews(String title) {
		News foundNews = null;
		for (News news : newsList) {
			if (news.titleEquals(title)) foundNews = news;
		}
		
		if (foundNews != null) {
			newsList.remove(foundNews);
		}
	}
	
	public void createComment(String articleTitle, String comment) {
		for (News news : newsList) {
			if (news.titleEquals(articleTitle)) {
				news.comments.add(comment);
			}
		}
	}
	
	public void deleteComment(String articleTitle, String aComment) {
		News targetArticle = null;
		for (News news : newsList) {
			if (news.titleEquals(articleTitle)) {
				int commentIndex = -1;
				
				for (int i = 0; i < news.comments.size(); i++) {
					if (news.comments.get(i).equals(aComment)) commentIndex = i;
				}
				
				if (commentIndex > -1) news.comments.remove(commentIndex);
			}
		}
		
	}
	
	public void save(File outFile) {
		byte[] outBytes = this.toXML().getBytes();
		FileOutputStream fOut = null;
		try {
			fOut = new FileOutputStream(outFile, false);
			fOut.write(outBytes);
			fOut.close();
		} catch (Exception e) {
			System.out.println("WARNING: Save of news data failed");
			if (fOut != null) {
				try {
					fOut.close();
				} catch (IOException ex) {
					System.out.println("WARNING: Unable to cose output stream");
				}
			}
		}
	}
	
	public void load(InputStream inFile) {
		String xmlRaw = loadFromFile(inFile);
		
		if (xmlRaw != null) {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			try {
				InputStream xmlInput = new StringBufferInputStream(xmlRaw);
				SAXParser saxParser = factory.newSAXParser();
				saxParser.parse(xmlInput, this);
			} catch (Throwable e) {
				System.out.println("ERROR: Unable to parse XML file for news");
				e.printStackTrace();
			}
		}
	}
	
	private String loadFromFile(InputStream is) {
		StringBuffer xmlData = new StringBuffer();
		BufferedReader bRead = null;
		try {
			bRead = new BufferedReader(new InputStreamReader(is));
			String line = "";
			while((line = bRead.readLine())!=null){
				xmlData.append(line.trim());
			}
		} catch (IOException e) {
			System.out.println("FATAL: IOException");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("FATAL: Exception");
			e.printStackTrace();
		} finally {
			if (bRead != null) {
				try {
					bRead.close();
				} catch (IOException e) {
					System.out.println("WARNING: Unable to cose input stream");
				}
			}
		}
		
		return xmlData.toString();
	}
	
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		if (qName.equals("story")) {
			// Add a new news
			newsList.add(new News());
		} else if (qName.equals("comment")) {
			newsList.get(newsList.size() -1).comments.add("");
		}
		this.lastElement = qName;
		elementTrace = elementTrace + "!" + qName;
	}
	
	public void characters(char ch[], int start, int length)
			throws SAXException {
		String sData = new String(ch, start, length);
		if (newsList.size() > 0) {
			newsList.get(newsList.size() -1).appendTo(lastElement,sData);
		}
	}
	
	private String toXML() {
		StringBuilder strReturn = new StringBuilder();
		strReturn.append("<?xml version='1.0' encoding='UTF-8'?><collection>");
		for (News news : newsList) {
			strReturn.append("<news><title>");
			strReturn.append(news.title);
			strReturn.append("</title><author>");
			strReturn.append(news.author);
			strReturn.append("</author><published>");
			if (news.published) {
				strReturn.append("true");
			} else {
				strReturn.append("false");
			}
			strReturn.append("</published><body>");
			strReturn.append(news.body);
			strReturn.append("</body>");
			strReturn.append("</body>");
			for (String comment : news.comments) {
				strReturn.append("<comment>");
				strReturn.append(comment);
				strReturn.append("</comment>");
			}
			strReturn.append("</news>");
			
		}

		strReturn.append("</collection>");
		return strReturn.toString();
	}
}
