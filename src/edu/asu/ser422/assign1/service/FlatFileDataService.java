package edu.asu.ser422.assign1.service;

import edu.asu.ser422.assign1.service.fileIO.*;
import edu.asu.ser422.assign1.model.*;

import java.io.*;
import javax.servlet.ServletContext;
import javax.servlet.http.*;
import com.google.gson.JsonSyntaxException;
import java.text.SimpleDateFormat;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class FlatFileDataService implements DataService {
	private FlatFileConfig thisConfig;
	private NewsFile newsFile;
	private NewsUsersFile usersFile;
	
	public FlatFileDataService() {
		this.thisConfig = null;
		this.newsFile = null;
		this.usersFile = null;
	}
	
	public FlatFileDataService(FlatFileConfig fileConfig) {
		this();
		
		// Hold onto servlet context so we can save files later
		this.thisConfig = fileConfig;
		
		// Blindly init data files with configuration data
		if (thisConfig.appearsValid()) {
			this.newsFile = new NewsFile(thisConfig.aContext.getResourceAsStream(fileConfig.newsPath));
			this.usersFile = new NewsUsersFile(thisConfig.aContext.getResourceAsStream(fileConfig.userPath));
		} else {
			this.newsFile = new NewsFile();
			this.usersFile = new NewsUsersFile();
		}
	}
	
	private void reload(FlatFile aFile, InputStream inStream) {
		aFile.load(inStream);
	}
	
	private void flush(FlatFile aFile, File destination) {
		aFile.save(destination);
	}
	
	// Implementations of interface methods are in order below
	// Users CRUD
	public void createUser(String uid, String role) {
		usersFile.createUser(uid,role);
	}
	
	public NewsUser getUser(String uid) {
		return usersFile.getUser(uid);
	}
	
	// News CRUD
	
	public News[] getNews() {
		List<News> lstReturn = new ArrayList<News>();
		
		return lstReturn.toArray(new News[0]);
	}
	
	public News[] getNewsByUid(String uid) {
		List<News> lstReturn = new ArrayList<News>();
		
		return lstReturn.toArray(new News[0]);
	}
	
	public News[] getNewsByTitle(String title) {
		List<News> lstReturn = new ArrayList<News>();
		
		// undo URL encoding of titles
		String searchTitle = title.replaceAll("_"," ");
		
		News[] resultSet = newsFile.getNews();
		
		for (News news : resultSet) {
			if (news.title.equals(searchTitle)) {
				lstReturn.add(news);
			}
		}
		
		return lstReturn.toArray(new News[0]);
	}
	
	public News[] getNewsByPublished(boolean isPublished) {
		List<News> lstReturn = new ArrayList<News>();
		News[] resultSet = newsFile.getNews();
		for (News news : resultSet) {
			if (news.isPublished()) {
				lstReturn.add(news);
			}
		}
		return lstReturn.toArray(new News[0]);		
	}
	
	public News[] getNewsByContext(String uid, String role) {
		List<News> lstReturn = new ArrayList<News>();
		News[] resultSet = newsFile.getNews();
		for (News news : resultSet) {
			if (news.isPublished()) {
				lstReturn.add(news);
			} else if (news.author.equals(uid)) {
				lstReturn.add(news);
			}
		}
		return lstReturn.toArray(new News[0]);		
	}
	
	public void createNews(News someNews) {
		newsFile.createNews(someNews);
	}
	
	public void deleteNews(String title) {
		newsFile.deleteNews(title);
	}
	
	// Comments CRUD
	public void createComment(String articleTitle, String comment) {
		newsFile.createComment(articleTitle, comment);
	}
	
	public void deleteComment(String articleTitle, String comment) {
		newsFile.deleteComment(articleTitle, comment);
	}
	
	// Used for debugging
	public String typeAsString() {
		return "FlatFile";
	}
}
