package edu.asu.ser422.assign1;

import edu.asu.ser422.assign1.service.*;
import edu.asu.ser422.assign1.model.*;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class NewsApp extends HttpServlet {

	private static String DATA_SOURCE_CONFIG;
	private static DAOFactory dataStore;
	
	public void init(ServletConfig config) throws ServletException {

		// if you forget this your getServletContext() will get a NPE!
		// Thank you Dr. Gary! I didn't forget it
		super.init(config);

		// Loads location names for data stores
		DATA_SOURCE_CONFIG = config.getInitParameter("DataSourcesProperties");
		
		if (DATA_SOURCE_CONFIG == null || DATA_SOURCE_CONFIG.length() == 0) {
			throw new ServletException();
		}
		
		// Pass properties file to data storage wrapper for init
		dataStore = new DAOFactory(getServletContext(),
									DATA_SOURCE_CONFIG);
									
	}

	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException {


		// Determine login state and pass that to views
		HttpSession session = request.getSession();
		String uid = (String) session.getAttribute("uid");
		if (uid != null) {
			request.setAttribute("uid",uid);
		} else {
			session.setAttribute("uid","");
		}
		
		String uRole = (String) session.getAttribute("role");
		if (uRole != null) {
			request.setAttribute("role",uRole);
		} else {
			session.setAttribute("role","");
		}
		
		// Re-using static routing from lab 1
		// Perform actions based on uri
		String routePath = request.getServletPath();
		if (routePath.equals("/index")) {
			doIndex(request,response);
		} else if (routePath.equals("/read")) {
			doRead(request,response);
		} else if (routePath.equals("/login")) {
			doLogin(request,response);
		} else if (routePath.equals("/edit")) {
			doEdit(request,response);
		} else if (routePath.equals("/rm")) {
			doRemove(request,response);
		} else if (routePath.equals("/comment")) {
			doComment(request,response);
		} else {
			
		}	
	}

	public void doPost(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException {
						
		this.doGet(request,response);			
	}
	
	private void doIndex(HttpServletRequest aRequest, HttpServletResponse aResponse) throws ServletException, IOException {
		
		// Prepare available article list for index view
		NewsDAO newsData = dataStore.getNewsDAO();
		String uid = (String) aRequest.getAttribute("uid");
		String uRole = (String) aRequest.getAttribute("role");
		News[] newsItems = newsData.getNews(uid,uRole);
		
		// Supply news list to view
		aRequest.setAttribute("news", newsItems);
		
		// Forward the request
		RequestDispatcher requestDispatcher = 
			aRequest.getRequestDispatcher("/index.jsp");
		requestDispatcher.forward(aRequest, aResponse);
		
	}
	private void doLogin(HttpServletRequest aRequest,HttpServletResponse aResponse) throws ServletException, IOException {
		
		// Get all the strings we need to do routing logic
		String uid = aRequest.getParameter("uid");
		String password = aRequest.getParameter("upw");
		String role = aRequest.getParameter("role");
		String action = aRequest.getParameter("action");
		String forwardTo = "/login.jsp";
		String forwardMessage = "";
		
		// Perform BL actions if action param was procided
		if (action != null) {
			
			// Get a handle to data storage so we can perform actions
			NewsUserDAO userData = dataStore.getNewsUserDAO();
			
			if (action.equals("login")) {
				NewsUser aUser = null;
				if (uid != null && uid.length() > 0) {
					aUser = userData.getUser(uid);
				}
				
				// If the uid and password are not null, and match
				// and the user exists, log the user in
				if (aUser != null && password != null) {
					if (aUser.uid.equals(password)) {
						HttpSession session = aRequest.getSession();
						session.setAttribute("uid",aUser.uid);
						session.setAttribute("role",aUser.role);
						forwardTo = "/index";
					} else {
						forwardMessage = "There was an error processing your request. Check your password and try again";
						forwardTo = "/login.jsp";
					}
				} else if (aUser != null) {
					forwardMessage = "Invalid password or bad user name, please try again";
					forwardTo = "/login.jsp";
				} else {
					// The user does not exist. change action to create and forward
					aRequest.setAttribute("action","create");
					forwardTo = "/login.jsp";
				}
			} else if (action.equals("create")) {
				
				// check for nulls before creating user
				if (uid != null && password != null && role != null) {
					NewsUser aUser = userData.getUser(uid);
					
					// aUser will be null if the user does not exist
					if (aUser == null) {
						userData.createUser(uid,role);
						HttpSession session = aRequest.getSession();
						session.setAttribute("uid",uid);
						session.setAttribute("role",role);
						forwardTo = "/index";
					}
				} else {
					forwardMessage = "Unable to create user. Provided data was not complete";
					forwardTo = "/login.jsp";
				}
			} else if (action.equals("logout")) {
				HttpSession session = aRequest.getSession();
				aRequest.setAttribute("uid","");
				aRequest.setAttribute("role","");
				session.invalidate();
				forwardTo = "/index";
			}
		}
		
		aRequest.setAttribute("message",forwardMessage);
		if (forwardTo.contains("/login")) {
			RequestDispatcher requestDispatcher = 
				aRequest.getRequestDispatcher(forwardTo);
			requestDispatcher.forward(aRequest, aResponse);
		} else {
			forwardTo = forwardTo.replaceFirst("/","");
			aResponse.sendRedirect(forwardTo);
		}
	}
	private void doRead(HttpServletRequest aRequest,HttpServletResponse aResponse) throws ServletException, IOException {
		
		// Get requested article from data store
		String newsTitle = aRequest.getParameter("title");
		if (newsTitle==null) newsTitle = "";
		NewsDAO newsData = dataStore.getNewsDAO();
		News[] newsItems = newsData.getNewsByTitle(newsTitle);
		// Present data to view
		if (newsItems != null && newsItems.length > 0) {
			// Best case, we found the article
			aRequest.setAttribute("news", newsItems);
		}
		
		// Forward the request
		RequestDispatcher requestDispatcher = 
			aRequest.getRequestDispatcher("/read.jsp");
		requestDispatcher.forward(aRequest, aResponse);
	}
	private void doEdit(HttpServletRequest aRequest,HttpServletResponse aResponse) throws ServletException, IOException {
		
		// Get relevant fields for this transaction
		String uid = (String) aRequest.getAttribute("uid");
		String role = (String) aRequest.getAttribute("role");
		String newsTitle = aRequest.getParameter("title");
		String newsPublished = aRequest.getParameter("published");
		String newsBody = aRequest.getParameter("body");
		String action = aRequest.getParameter("action");
		
		// convert published string to a boolean
		boolean isPublished = false;
		if (newsPublished != null) {
			isPublished = true;
		}
		
		// Execute a save if action is set
		if (action != null && action.equals("save")) {
			
			NewsDAO newsData = dataStore.getNewsDAO();
			News newNews = new News(newsTitle, uid, newsBody, isPublished);
			newsData.createNews(newNews);
			
			// Ask client to return to index
			aResponse.sendRedirect("index");
		} else if (newsTitle != null && newsTitle.length() > 0) {
			
			// Provide requested article to editor view
			NewsDAO newsData = dataStore.getNewsDAO();
			News[] newsResults = newsData.getNewsByTitle(newsTitle);
			aRequest.setAttribute("news",newsResults);
			
			// forward to editor view
			RequestDispatcher requestDispatcher = 
				aRequest.getRequestDispatcher("/edit.jsp");
			requestDispatcher.forward(aRequest, aResponse);
		} else {
			// Forward the request
			RequestDispatcher requestDispatcher = 
				aRequest.getRequestDispatcher("/edit.jsp");
			requestDispatcher.forward(aRequest, aResponse);
		}
		
	}

	private void doRemove(HttpServletRequest aRequest,HttpServletResponse aResponse) throws ServletException, IOException {
		
		// Get the data we need for this transaction
		String newsTitle = aRequest.getParameter("title");
		String newsComment = aRequest.getParameter("comment");
		String actionConfirmed = aRequest.getParameter("confirm");
		String uid = (String) aRequest.getAttribute("uid");
		
		String forwardMessage = "";
		String forwardTo = "/confirm.jsp";
		
		// Forward to confirmation page if confirm param is null
		if (actionConfirmed==null) {
			
			// Set message according to what the user is trying to delete
			if (newsComment != null && newsTitle != null) {
				
				// Request is to delete a comment
				forwardMessage = "You are about to delete the comment;" + newsComment;
				forwardTo = "/confirm.jsp";
			} else if (newsTitle != null) {
				
				// Request is for deletion of an article
				forwardMessage = "You are about to delete the article; " + newsTitle;
				forwardTo = "/confirm.jsp";
			} else {
				forwardMessage = "Unable to fulfill your request. A required data firld was missing";
				forwardTo = "/index";
			}
		} else {
			
			// 'confirm' param has been set, so lets execute the transaction
			if (newsComment != null && newsTitle != null) {
				
				// Request is to delete a comment
				NewsDAO newsData = dataStore.getNewsDAO();
				newsData.deleteComment(newsTitle, newsComment);
				forwardTo = "/read";
			} else if (newsTitle != null) {
				
				// Request is for deletion of an article
				NewsDAO newsData = dataStore.getNewsDAO();
				newsData.deleteNews(newsTitle);
				forwardTo = "/index";
			} else {
				forwardMessage = "Unable to fulfill your request. A required data firld was missing";
				forwardTo = "/index";
			}
		}
		
		// Forward the request
		aRequest.setAttribute("message", forwardMessage);
		RequestDispatcher requestDispatcher = 
			aRequest.getRequestDispatcher(forwardTo);
		requestDispatcher.forward(aRequest, aResponse);
	}
	private void doComment(HttpServletRequest aRequest,HttpServletResponse aResponse) throws ServletException, IOException {
		String newsTitle = aRequest.getParameter("title");
		String newsComment = aRequest.getParameter("comment");
		
		if (newsTitle != null && newsComment != null) {
			NewsDAO newsData = dataStore.getNewsDAO();
			newsData.createComment(newsTitle, newsComment);
		}
		RequestDispatcher requestDispatcher = 
			aRequest.getRequestDispatcher("/read");
		requestDispatcher.forward(aRequest, aResponse);
		
	}
	
	/**
	 * Not used
	 */
	public void destroy() {
		// do nothing because Dr. Gary says you shouldm't ever need to
		// ... unless you are doing it wrong.
	}
}
