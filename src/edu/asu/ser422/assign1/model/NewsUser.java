package edu.asu.ser422.assign1.model;

public class NewsUser {
	public String uid;
	public String role;
	
	public NewsUser() {
		this.uid = "";
		this.role = "";
	}
	
	public NewsUser(String newUid, String newRole) {
		this.uid = newUid;
		this.role = newRole;
	}
}
