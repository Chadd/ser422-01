package edu.asu.ser422.assign1.model;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class News {
	public int id;
	public String title;
	public String author;
	public boolean published;
	public String body;
	public List<String> comments;
	
	/**
	 * Create an empty stort object
	 */
	public News() {
		this("","","",false);
	}
	
	/**
	 * Create a News object with the provided data
	 * @param aTitle The title of the story
	 * @param aAuthor The UID of the author of this story
	 * @param aBody The content of the story
	 * @isPublished If True, the story will be visible in the story index
	 * 			otherwise, the story will only be visible by it's author
	 */
	public News(String aTitle, String aAuthor, String aBody, boolean isPublished) {
		this.id = 0;
		this.title = aTitle;
		this.author = aAuthor;
		this.published = isPublished;
		this.body = aBody;
		comments = new ArrayList<String>();
	}
	
	/**
	 * append provided data to the associated tag element
	 * @param element The query string or XML tag name
	 * @param data The content to be appended from the associated tag
	 */
	public void appendTo(String element, String data) {
		if (element.equals("title")) {
			this.title = this.title + data.trim();
		} else if (element.equals("author")) {
			this.author = this.author + data.trim();
		} else if (element.equals("body")) {
			this.body = this.body + data.trim();
		} else if (element.equals("published")) {
			if (data.contains("true")) this.published = true;
		} else if (element.equals("comment")) {
			String newComment = this.comments.get(this.comments.size() -1);
			newComment = newComment + data.trim();
			this.comments.set(comments.size() -1, newComment);
		}
	}
	
	public boolean isPublished() {
		return this.published;
	}
	
	/**
	 * Determines if the provided title matches the encoded form of
	 * an articles title. IT IS IMPORTANT that the provided name is
	 * already encoded before invoking this method.
	 * @aTitle An encoded title to be rested against. Encoding in this
	 * 			context is running your title through the ("\\W","_")
	 * 			regex, which will replace and non-word characters with
	 * 			underscores
	 * @return True if the provided encoded title is a match
	 */
	public boolean titleEquals(String aTitle) {
		boolean bolReturn = false;
		if (encodeTitle(this.title).equals(encodeTitle(aTitle))) {
			bolReturn = true;
		}
		return bolReturn;
	}
	
	public String getEncodedTitle() {
		return encodeTitle(this.title);
	}
	/**
	 * encodes a provided title for output on the URL query string
	 * @return The encoded string
	 */
	private String encodeTitle(String aTitle) {
		String sRerurn = aTitle.replaceAll("\\W","_");
		return sRerurn;
	}
}

